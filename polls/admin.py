from django.contrib import admin
from .models import Pregunta, Opcion

class OpcionEnLinea(admin.TabularInline):
    model = Opcion
    extra = 3

class PreguntaAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, { 'fields': ['texto_pregunta'] }),
        ('Información sobre la fecha', { 'fields': ['fecha_pub'], 'classes': ['collapse'] })
    ]

    inlines = [OpcionEnLinea]
    list_display = ('texto_pregunta', 'fecha_pub', 'fue_publicada_recientemente')
    list_filter = ['fecha_pub']
    search_fields = ['texto_pregunta']


admin.site.register(Pregunta, PreguntaAdmin)
