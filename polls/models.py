import datetime
from django.db import models
from django.utils import timezone

class  Pregunta(models.Model):
    texto_pregunta = models.CharField(max_length=200)
    fecha_pub = models.DateTimeField('fecha publicacion') #Esto permite indicar un nombre leible por humanos para este campo

    def __str__(self):
        return self.texto_pregunta
    
    def fue_publicada_recientemente(self):
        ahora = timezone.now()
        return ahora >= self.fecha_pub >= ahora - datetime.timedelta(days=1)
    
    fue_publicada_recientemente.admin_order_field = 'fecha_pub'
    fue_publicada_recientemente.boolean = True
    fue_publicada_recientemente.short_description = '¿Publicada recientemente?'

class Opcion(models.Model):
    pregunta = models.ForeignKey(Pregunta, on_delete=models.CASCADE)
    texto_opcion = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)

    def __str__(self):
        return self.texto_opcion


