from django.test import TestCase
import datetime
from django.utils import timezone
from django.urls import reverse

from .models import Pregunta

class TestsModeloPregunta(TestCase):

    def test_fue_publicada_recientemente_con_pregunta_a_futuro(self):
        """ 
        fue_publicada_recientemente() retorna False para preguntas 
        cuya fecha_pub esta en el futuro.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        pregunta_futura = Pregunta(fecha_pub = time)
        self.assertIs(pregunta_futura.fue_publicada_recientemente(), False)
    
    def test_fue_publicada_recientemente_con_pregunta_antigua(self):
        """
        fue_publicada_recientemente() retorna False para preguntas cuya fecha_pub
        es anterior a 1 día.
        """
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pregunta_antigua = Pregunta(fecha_pub=time)
        self.assertIs(pregunta_antigua.fue_publicada_recientemente(), False)
        
    def test_fue_publicada_recientemente_con_pregunta_reciente(self):
        """
        fue_publicada_recientemente() retorna True para preguntas cuya fecha_pub
        está dentro del último dia.
        """
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        pregunta_reciente = Pregunta(fecha_pub=time)
        self.assertIs(pregunta_reciente.fue_publicada_recientemente(), True)

def crear_una_pregunta(texto_pregunta, dias):
    """
    Crea una pregunta con el 'texto_pregunta' ingresado y publicada el número de 'dias'
    ingresado desde ahora (negativo para preguntas publicadas en el pasado, positivo para
    preguntas que todavia no han sido publicadas).
    """
    time = timezone.now() + datetime.timedelta(days=dias)
    return Pregunta.objects.create(texto_pregunta = texto_pregunta, fecha_pub = time)

class PreguntaIndexViewTests(TestCase):
    def test_no_questions(self):
        """
        Si no existen preguntas, un mensaje apropiado es mostrado.
        """
        respuesta = self.client.get(reverse('polls:index'))
        self.assertEqual(respuesta.status_code, 200)
        self.assertContains(respuesta, "No hay encuestas disponibles.")
        self.assertQuerysetEqual(respuesta.context['ultimas_preguntas'], [])

    def test_pregunta_antigua(self):
        """
        Preguntas con una fecha_pub en el pasado, son mostradas en el index.
        """
        crear_una_pregunta(texto_pregunta = "Pregunta antigua.", dias = -30)
        respuesta = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(respuesta.context['ultimas_preguntas'], ['<Pregunta: Pregunta antigua.>'])

    def test_pregunta_futura(self):
        """
        Precuntas con una fecha_pub en el futuro no son mostradas en el index. 
        """
        crear_una_pregunta(texto_pregunta = "Pregunta futura.", dias = 30)
        respuesta = self.client.get(reverse('polls:index'))
        self.assertContains(respuesta, "No hay encuestas disponibles.")
        self.assertQuerysetEqual(respuesta.context['ultimas_preguntas'], [])

    def test_pregunta_futura_y_pregunta_antigua(self):
        """
        Incluso si ambas antiguas y futuras preguntas existen, solo las preguntas antiguas son mostradas.
        """
        crear_una_pregunta(texto_pregunta = "Pregunta antigua.", dias = -30)
        crear_una_pregunta(texto_pregunta = "Pregunta futura.", dias = 30)
        respuesta = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(respuesta.context['ultimas_preguntas'], ['<Pregunta: Pregunta antigua.>'])

    def test_dos_preguntas_antiguas(self):
        """
        La pagina index de preguntas puede mostrar multiples preguntas.
        """
        crear_una_pregunta(texto_pregunta = "Pregunta antigua 1.", dias = -30)
        crear_una_pregunta(texto_pregunta = "Pregunta antigua 2.", dias = -5)
        respuesta = self.client.get(reverse('polls:index'))
        self.assertQuerysetEqual(respuesta.context['ultimas_preguntas'], ['<Pregunta: Pregunta antigua 2.>', '<Pregunta: Pregunta antigua 1.>'])



