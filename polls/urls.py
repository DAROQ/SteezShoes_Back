from django.urls import path

from . import views

app_name = 'polls' # Namespace para los paths, identifica las rutas de una aplicación especifica

urlpatterns = [
    # path('', views.index, name='index'), (1)
    # path('<int:id_pregunta>/', views.detail, name='detalle'), (1)
    # path('<int:id_pregunta>/resultados/', views.results, name='resultados'), (1)
    # path('<int:id_pregunta>/votar/', views.vote, name='votar') (1)
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detalle'),
    path('<int:pk>/resultados/', views.ResultsView.as_view(), name='resultados'),
    path('<int:id_pregunta>/votar/', views.vote, name='votar')
]