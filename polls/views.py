from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.views import generic
from django.utils import timezone

# Importaciones para el API REST
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, permissions
from .serializers import UserSerializer, GroupSerializer

# Modelos
from .models import Pregunta, Opcion

# def index(request):(1)
class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'ultimas_preguntas'
    def get_queryset(self):
        # """ Retorna las ultimas cinco preguntas publicadas """ (4)
        # return  Pregunta.objects.order_by('-fecha_pub')[:5] (4)
        """ 
        Retorna las ultimas cinco preguntas publicadas (sin incluir aquellas
        que estan configuradas para ser publicadas en el futuro). 
        """
        return Pregunta.objects.filter(fecha_pub__lte = timezone.now()).order_by('-fecha_pub')[:5]
    # ultimas_preguntas = Pregunta.objects.order_by('-fecha_pub')[:5] (3)
    #response = ', '.join([p.texto_pregunta for p in ultimas_preguntas]) (1)
    #return HttpResponse(response) (1)
    #template = loader.get_template('polls/index.html') (2)
    # context = { (3)
    #     'ultimas_preguntas': ultimas_preguntas (3)
    # } (3)
    #return HttpResponse(template.render(context, request)) (2)
    # return render(request, 'polls/index.html', context) (3)

# def detail(request, id_pregunta): (1)
class DetailView(generic.DetailView):
    model = Pregunta
    template_name = 'polls/detail.html'
    # try:
    #     pregunta = Pregunta.objects.get(pk=id_pregunta) (1)
    # except Pregunta.DoesNotExist: (1)
    #     raise Http404("La pregunta no existe") (1)
    # return render(request, 'polls/detail.html', {'pregunta': pregunta}) (1)
    # pregunta = get_object_or_404(Pregunta, pk=id_pregunta) (2)
    # return render(request, 'polls/detail.html', {'pregunta': pregunta}) (2)

# def results(request, id_pregunta): (1)
class ResultsView(generic.DetailView):
    model = Pregunta
    template_name = 'polls/results.html'
    # response = "Estas mirando los resultados de la pregunta %s." (1)
    # return HttpResponse(response % id_pregunta) (1) 
    # pregunta = get_object_or_404(Pregunta, pk=id_pregunta) (2)
    # return render(request, 'polls/results.html',{ 'pregunta': pregunta }) (2)

def vote(request, id_pregunta):
    pregunta = get_object_or_404(Pregunta, pk=id_pregunta)
    try:
        opcion_elegida = pregunta.opcion_set.get(pk=request.POST['opcion'])
    except (KeyError, Opcion.DoesNotExist):
            # Volver a mostrar el formulario de votación
            return render(request, 'polls/detail.html', {
                'pregunta': pregunta,
                'mensaje_error': "No seleccionaste una opcion."
            })
    else:
        opcion_elegida.votos += 1
        opcion_elegida.save()
        # Siempre retornar un HttpResponseRedirect despues de tratar correctamente con una petición POST
        # Esto previene que los datos sean posteados dos veces si el usuario usa el botón "atras" del navegador
        return HttpResponseRedirect(reverse('polls:resultados', args=(pregunta.id,))) # La coma despues de 'pregunta.id' es fundamental, simpre incluirla para que args se procese como un iterable
    # return HttpResponse("Estas votando en la pregunta %s." % id_pregunta) (1)

class UserViewSet(viewsets.ModelViewSet):
    """
    API Endpoint que permite ver y editar usuarios
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

class GroupViewSet(viewsets.ModelViewSet):
    """
    API Endpoint que permite ver y editar grupos
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]

