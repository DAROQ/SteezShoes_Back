from django.contrib import admin

from .models import Zapato, Detalle

admin.site.register(Zapato)
admin.site.register(Detalle)

