# Generated by Django 3.1.7 on 2021-03-20 03:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Detalle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=900)),
                ('detail_img_left', models.ImageField(blank=True, upload_to='imagenes')),
                ('detail_img_right', models.ImageField(blank=True, upload_to='imagenes')),
                ('detail_img', models.ImageField(blank=True, upload_to='imagenes')),
            ],
        ),
        migrations.AddField(
            model_name='zapato',
            name='detalle',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='shoes.detalle'),
        ),
    ]
