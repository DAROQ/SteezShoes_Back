from django.db import models

class Detalle(models.Model):
    descripcion = models.CharField(max_length=900)
    detail_img_left = models.ImageField(upload_to = 'imagenes', blank = True)
    detail_img_right = models.ImageField(upload_to = 'imagenes', blank = True)
    detail_img = models.ImageField(upload_to = 'imagenes', blank = True)

    def __str__(self):
        return self.descripcion

class Zapato(models.Model):
    modelo = models.CharField(max_length=40)
    marca = models.CharField(max_length=20)
    precio = models.IntegerField()
    foto = models.ImageField(upload_to = 'imagenes')
    detalle = models.OneToOneField(Detalle, on_delete = models.CASCADE, null = True)

    def __str__(self):
        return self.modelo