from rest_framework import serializers
from .models import Zapato, Detalle

# class ZapatoSerializer(serializers.Serializer): (1)
#     id = serializers.IntegerField(read_only = True) (1)
#     modelo = serializers.CharField(max_length = 40) (1)
#     marca = serializers.CharField(max_length=20) (1)
#     precio = serializers.IntegerField() (1)
#     foto = serializers.ImageField() (1)

#     def create(self, validated_data): (1)
#         """
#         Crea y retorna una nueva instancia de `Zapato`, dados los datos validados. (1)
#         """
#         return Zapato.objects.create(**validated_data) (1)

#     def update(self, instance, validated_data): (1)
#         """
#         Actualiza y retorna una instancia existente de `Zapato`, dados los datos validados. (1)
#         """
#         instance.modelo = validated_data.get('modelo', instance.modelo) (1)
#         instance.marca = validated_data.get('marca', instance.marca) (1)
#         instance.precio = validated_data.get('precio', instance.precio) (1)
#         instance.foto = validated_data.get('foto', instance.foto) (1)
#         instance.save() (1)
#         return instance (1)

# class ZapatoSerializer(serializers.ModelSerializer): (2)
class ZapatoSerializer(serializers.ModelSerializer):
    detalleURL = serializers.HyperlinkedIdentityField(view_name='steez:detalle-detail')
    class Meta:
        model = Zapato
        fields = ['id', 'modelo', 'marca', 'precio', 'foto', 'detalleURL', 'detalle']

# class DetalleSerializer(serializers.ModelSerializer):
class DetalleSerializer(serializers.HyperlinkedModelSerializer):    
    class Meta:
        model = Detalle
        fields = ['id', 'descripcion', 'detail_img_left', 'detail_img_right', 'detail_img']