from django.urls import path, include

# REST Framework
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter
from . import views

# Declarar namespace de la app
app_name = 'steez' # Se debe usar cada vez que se referencie un path por su name. Ejemplo: el path 'zapatos/' se referencia como 'steez:lista-zapatos'

# Creación de vistas a partir de los viewsets definidos en views.py (3)
# ListaZapatos = views.ZapatoViewSet.as_view({ (3)
#     'get': 'list', (3)
#     'post': 'create' (3)
# }) (3)

# DetalleZapato = views.ZapatoViewSet.as_view({ (3)
#     'get': 'retrieve', (3)
#     'put': 'update', (3)
#     'patch': 'partial_update', (3)
#     'delete': 'destroy' (3)
# }) (3)

# ListaDetalles = views.DetalleViewSet.as_view({ (3)
#     'get': 'list' (3)
# }) (3)

# DetalleDetail = views.DetalleViewSet.as_view({ (3)
#     'get': 'retrieve' (3)
# }) (3)

# urlpatterns = [ (3)
    # path('', views.index, name='index'),(1)
    # path('', views.steez_root, name='api-root'), (3)
    # path('zapatos/', views.lista_zapatos, name='list'), (1)
    # path('zapatos/', views.ListaZapatos.as_view(), name='lista-zapatos'), (2)
    # path('zapatos/', ListaZapatos, name = 'lista-zapatos'), (3)
    # path('zapatos/<int:pk>/', views.detalle_zapato, name='detail') (1)
    # path('zapatos/<int:pk>/', views.DetalleZapato.as_view(), name='zapato-detail'),(2)
    # path('zapatos/<int:pk>/',DetalleZapato, name ='zapato-detail'), (3)
    # path('detalles/', views.ListaDetalles.as_view(), name='lista-detalles'), (2)
    # path('detalles/', ListaDetalles, name = 'lista-detalles'), (3)
    # path('detalles/<int:pk>/', views.DetalleDetail.as_view(), name = 'detalle-detail') (2)
    # path('detalles/<int:pk>/', DetalleDetail, name = 'detalle-detail') (3)
# ] (3)

# urlpatterns = format_suffix_patterns(urlpatterns) (3)

router = DefaultRouter()
router.register(r'zapatos', views.ZapatoViewSet)
router.register(r'detalles', views.DetalleViewSet)

urlpatterns = [
    path('', include(router.urls)),
]