from django.http import HttpResponse, JsonResponse, Http404 
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import mixins, generics
from rest_framework.reverse import reverse
from rest_framework import viewsets

from .models import Zapato, Detalle
from .serializers import ZapatoSerializer, DetalleSerializer

def index(request):
    return HttpResponse("Estas en el index de shoes.") 

# @csrf_exempt (1)
# @api_view(['GET', 'POST']) (2)
# def lista_zapatos(request, format = None): (2)
# class ListaZapatos(APIView): (3)
# class ListaZapatos(mixins.ListModelMixin, mixins.CreateModelMixin, generics.GenericAPIView): (4)
# class ListaZapatos(generics.ListCreateAPIView):(5)
#     """
#     Lista todos los zapatos, o crea un nuevo zapato (5)
#     """ 
#     queryset = Zapato.objects.all() (5)
#     serializer_class = ZapatoSerializer (5)

    # if request.method == 'GET': (2)
    # def get(self, request, format = None): (3)
    # def get (self, request, *args, **kwargs): (4)
        # zapatos = Zapato.objects.all() (3)
        # serializer = ZapatoSerializer(zapatos, many = True) (3)
        # return JsonResponse(serializer.data, safe = False) (1)
        # return Response(serializer.data) (3)
        # return self.list(request, *args, **kwargs) (4)
    
    # elif request.method == 'POST':(2)
    # def post(self, request, format = None): (3)
    # def post (self, request, *args, **kwargs): (4)
        # data = JSONParser().parse(request) (1)
        # serializer = ZapatoSerializer(data = request.data) (3)
        # if serializer.is_valid(): (3)
        #     serializer.save() (3)
            # return JsonResponse(serializer.data, status = 201) (1)
            # return Response(serializer.data, status = status.HTTP_201_CREATED) (3)
        # return JsonResponse(serializer.errors, status = 400) (1)
        # return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST) (3)
        # return self.create(request, *args, **kwargs) (4)

# @csrf_exempt (1)
# @api_view(['GET', 'PUT', 'DELETE']) (2)
# def detalle_zapato(request, pk, format = None): (2)
# class DetalleZapato(APIView): (3)
# class DetalleZapato(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.DestroyModelMixin, generics.GenericAPIView): (4)
# class DetalleZapato(generics.RetrieveUpdateDestroyAPIView): (5)
#     """
#     Obtiene, actualiza o elimina un zapato (5)
#     """
#     queryset = Zapato.objects.all() (5)
#     serializer_class = ZapatoSerializer (5)

    # def get_object(self, pk): (3)
    #     try: (3)
            # zapato = Zapato.objects.get(pk = pk) (2)
            # return Zapato.objects.get(pk = pk) (3)
        # except Zapato.DoesNotExist: (3)
            # return HttpResponse(status = 404) (1)
            # return Response(status = status.HTTP_404_NOT_FOUND) (2)
            # raise Http404 (3)
    
    # if request.method == 'GET': (2)
    # def get(self, request, pk, format = None): (3)
    # def get(self, request, *args, **kwargs): (4)
        # zapato = self.get_object(pk) (3)
        # serializer = ZapatoSerializer(zapato) (3)
        # return JsonResponse(serializer.data) (1)
        # return Response(serializer.data) (3)
        # return self.retrieve(request, *args, **kwargs) (4)
    
    # elif request.method == 'PUT':(2)
    # def put(self, request, pk, format = None): (3)
    # def put(self, request, *args, **kwargs): (4)
        # data = JSONParser().parse(request) (1)
        # zapato = self.get_object(pk) (3)
        # serializer = ZapatoSerializer(zapato, data = request.data) (3)
        # if serializer.is_valid(): (3)
        #     serializer.save() (3)
            # return JsonResponse(serializer.data) (1)
            # return Response(serializer.data) (3)
        # return JsonResponse(serializer.errors, status = 400) (1)
        # return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST) (3)
        # return self.update(request, *args, **kwargs) (4)
    
    # elif request.method == 'DELETE':(2)
    # def delete(self, request, pk, format = None): (3)
    # def delete(self, request, *args, **kwargs): (4)
        # zapato = self.get_object(pk) (3)
        # zapato.delete() (3)
        # return HttpResponse(status = 204) (1)
        # return Response(status = status.HTTP_204_NO_CONTENT) (3)
        # return self.destroy(request, *args, **Kwargs) (4)

# class ListaDetalles(generics.ListAPIView): (5)
#     queryset = Detalle.objects.all() (5)
#     serializer_class = DetalleSerializer (5)

# class DetalleDetail(generics.RetrieveAPIView): (5)
#     queryset = Detalle.objects.all() (5)
#     serializer_class = DetalleSerializer (5)

class ZapatoViewSet(viewsets.ModelViewSet):
    """
    Este viewset automaticamente provee las acciones `listar`, `crear`, `obtener`,
    `actualizar` y `destruir`.
    """
    queryset = Zapato.objects.all()
    serializer_class = ZapatoSerializer

class DetalleViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Este viewset automaticamente provee acciones para 'listar' y 'obtener'
    """
    queryset = Detalle.objects.all()
    serializer_class = DetalleSerializer



@api_view(['GET'])
def steez_root(request, format = None):
    """
    La raiz del API de steez
    """
    return Response({
        'zapatos': reverse('steez:lista-zapatos', request = request, format = format),
        'detalles': reverse('steez:lista-detalles', request = request, format = format)
    })